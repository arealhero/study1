# Study

### Setup

Clone the repository

```sh
$ git clone https://gitlab.com/arealhero/study
```

Install NodeJS dependencies

```sh
$ cd study/backend
$ yarn install
```

Launch the server

```sh
$ cd ..
$ node backend/src/Server.js
```

Open your browser and go to `http://localhost`


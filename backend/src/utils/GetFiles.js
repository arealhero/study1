const fs = require('fs')

module.exports = function GetFiles(path) {
  return fs
    .readdirSync(path)
    .filter((file) => {
      let split = file.split('.')
      return split.length > 1 && split[split.length - 1] === 'tex'
    })
    .sort((a, b) => {
      let aNum = parseInt(a.split('.')[0])
      let bNum = parseInt(b.split('.')[0])

      if (aNum < bNum) {
        return -1
      } else if (aNum > bNum) {
        return 1
      } else {
        return 0
      }
    })
}

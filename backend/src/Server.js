const express = require('express')
const fs = require('fs')

const GetFiles = require('./utils/GetFiles')
const GetRandomInt = require('./utils/GetRandomInt')

const PORT = 80
const DATA_PATH = 'data'
const PUBLIC_PATH = 'public'

const app = express()

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*')
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  )
  next()
})

app.use((req, res, next) => {
  console.log(`GET ${req.url}`)
  next()
})

app.use('/styles', express.static(__dirname + '/../../public/styles'))
app.use('/img', express.static(__dirname + '/../../public/img'))
app.get('/', (req, res) => {
  let path = DATA_PATH + '/algebra/theorems'
  let filenames = GetFiles(path)

  let header = fs.readFileSync('./public/header.html')
  let setup = fs.readFileSync('./public/setup.tex')
  let questions = `<h2>Теоремы</h2><ul>`

  for (let i = 0; i < filenames.length; ++i) {
    let filename = filenames[i]
    let content = fs.readFileSync(path + '/' + filename).toString()
    let title = content.split('\n')[0]
    title = title.substring(4, title.length - 5)

    questions += `<li><a href="/algebra/theorems/${i + 1}">${title}</a></li>`
  }

  questions += '</ul><h2>Определения</h2><ul>'

  path = DATA_PATH + '/algebra/definitions'
  filenames = GetFiles(path)

  for (let i = 0; i < filenames.length; ++i) {
    let filename = filenames[i]
    let content = fs.readFileSync(path + '/' + filename).toString()
    let title = content.split('\n')[0]
    title = title.substring(4, title.length - 5)

    questions += `<li><a href="/algebra/definitions/${i + 1}">${title}</a></li>`
  }

  questions += '</ul><br><a href="/algebra/random">Рандомный вопрос</a>'

  let footer = fs.readFileSync('./public/footer.html')

  res.send(header + setup + questions + footer)
})

app.get('/algebra/theorems/:id', (req, res) => {
  const theoremsPath = DATA_PATH + '/algebra/theorems'

  const id = req.params.id - 1
  const filenames = GetFiles(theoremsPath)

  if (id >= filenames.length) {
    res.send('Invalid id')
    return
  }

  const header = fs.readFileSync(PUBLIC_PATH + '/header.html')
  const homeLink = '<a href="/">Список вопросов</a><hr>'
  const setup = fs.readFileSync(PUBLIC_PATH + '/setup.tex')
  const content = fs.readFileSync(theoremsPath + `/${filenames[id]}`)
  const footer = fs.readFileSync(PUBLIC_PATH + '/footer.html')

  res.send(header + homeLink + setup + content + footer)
})

app.get('/algebra/definitions/:id', (req, res) => {
  const theoremsPath = DATA_PATH + '/algebra/definitions'

  const id = req.params.id - 1
  const filenames = GetFiles(theoremsPath)

  if (id >= filenames.length) {
    res.send('Invalid id')
    return
  }

  const header = fs.readFileSync(PUBLIC_PATH + '/header.html')
  const homeLink = '<a href="/">Список вопросов</a><hr>'
  const setup = fs.readFileSync(PUBLIC_PATH + '/setup.tex')
  const content = fs.readFileSync(theoremsPath + `/${filenames[id]}`)
  const footer = fs.readFileSync(PUBLIC_PATH + '/footer.html')

  res.send(header + homeLink + setup + content + footer)
})

app.get('/algebra/random', (req, res) => {
  const header = fs.readFileSync('./public/header.html')
  const homeLink = '<a href="/">Список вопросов</a><hr>'
  const setup = fs.readFileSync('./public/setup.tex')
  const random = fs.readFileSync('./public/random.html')
  const footer = fs.readFileSync('./public/footer.html')

  res.send(header + homeLink + setup + random + footer)
})

app.get('/api/v1/random', (req, res) => {
  let questionPath = DATA_PATH
  // if (GetRandomInt(2) == 1) {
  questionPath += '/algebra/theorems'
  // } else {
  //   questionPath += '/algebra/definitions'
  // }

  const filenames = GetFiles(questionPath)

  let id = GetRandomInt(filenames.length)
  const question = fs.readFileSync(questionPath + '/' + filenames[id])

  res.send(question)
})

app.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`)
})
